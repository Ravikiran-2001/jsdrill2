function findLastCar(inventory) {
  //creating function called findLastCar
  if (Array.isArray(inventory)) {
    //checking whether given inventory is array or not
    return inventory[inventory.length - 1]; //returning last index value
  } else {
    return "Invalid array";
  }
}
module.exports = findLastCar; //exporting function
