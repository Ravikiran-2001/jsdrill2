//creating a function 
function findCarById(inventory, id) {
  if (Array.isArray(inventory)) {
    //checking whether given inventory is array or not
    let car = inventory.find(function (car) {
      //finding id using array find method
      return car.hasOwnProperty("id") && car.id === id;
    });
    return car;
  } else {
    return "invalid array";
  }
}
module.exports = findCarById; //exporting function
