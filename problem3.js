function sortCarModel(inventory) {
  //creating a function
  if (Array.isArray(inventory) && inventory[0].hasOwnProperty("car_model")) {
    //checking whether given inventory is array or not
    let carModel = inventory.map(function (car) {
      //extracting model using map
      return car.car_model;
    });
    carModel.sort(); //sorting returned array
    return carModel;
  } else {
    return "Invalid string";
  }
}
module.exports = sortCarModel; //exporting function
