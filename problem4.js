function allCarYear(inventory) {
  //creating function
  if (Array.isArray(inventory) && inventory[0].hasOwnProperty("car_year")) {
    //checking whether given inventory is array or not
    let carYear = inventory.map(function (car) {
      //extracting year using map
      return car.car_year;
    });
    return carYear;
  } else {
    return "Invalid Array";
  }
}
module.exports = allCarYear; //exporting function
