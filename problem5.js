function olderCars(carYear) {
  //creating function
  if (Array.isArray(carYear)) {
    //checking whether given carYear is array or not
    let year = carYear.filter(function (car) {
      //filtering out years before 2000
      return car < 2000;
    });
    console.log(`length of array ${year.length}`);
    return year;
  } else {
    return "Invalid Array";
  }
}
module.exports = olderCars; //exporting function
