function filterCarBrand(inventory) {
  //creating function
  if (Array.isArray(inventory)) {
    //checking whether given carYear is array or not
    let carBrand = inventory.filter(function (car) {
      //filtering all Audi and Bmw cars
      return (
        car.hasOwnProperty("car_make") &&
        (car.car_make === "Audi" || car.car_make === "BMW")
      );
    });
    return carBrand;
  } else {
    return "Invalid Array";
  }
}
module.exports = filterCarBrand; //exporting function
