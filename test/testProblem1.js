let inventory = require("../inventory"); //importing inventory array
let findCarById = require("../problem1"); //importing function of problem1 file
let carDetails = findCarById(inventory, 33); //calling the function
if (typeof carDetails === "string") {
  //checking whether returned variable is string or not
  return carDetails;
} else {
  console.log(
    `Car 33 is a ${carDetails.car_year} ${carDetails.car_make} ${carDetails.car_model}`
  );
}
