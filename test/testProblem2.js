let inventory = require("../inventory"); //importing inventory array
let findLastCar = require("../problem2"); //importing function of problem2 file
let lastCarDetails = findLastCar(inventory); //calling the function
if (typeof lastCarDetails === "string") {
  //checking whether returned variable is string or not
  console.log(lastCarDetails);
} else {
  console.log(
    `Last car is a ${lastCarDetails.car_make} ${lastCarDetails.car_model}`
  );
}
